class CreateSlides < ActiveRecord::Migration[5.0]
  def change
    create_table :slides do |t|
      t.string :name
      t.text :desc_left
      t.text :desc_right

      t.timestamps
    end
  end
end
