Rails.application.routes.draw do
  namespace :admin do
    resources :slides
    root 'slides#index'
  end
  resources :slides
  root 'slides#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
