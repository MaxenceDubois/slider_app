class SlidesController < ApplicationController
  before_action :set_slide, only: [:show]

  # GET /slides
  # GET /slides.json
  def index
    @slides = Slide.publication.min_publication.max_publication
  end

  # GET /slides/1
  # GET /slides/1.json
  def show
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_slide
      @slide = Slide.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def slide_params
      params.require(:slide).permit(:name, :desc_left, :desc_right, :picture, :published, :published_from, :published_to)
    end
end
