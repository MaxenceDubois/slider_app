json.extract! slide, :id, :name, :desc_left, :picture, :desc_right, :created_at, :updated_at
json.url slide_url(slide, format: :json)
