json.array!(@slides) do |slide|
  json.extract! slide, :name, :desc_left, :picture, :desc_right
  json.url slide_url(slide, format: :json)
end
